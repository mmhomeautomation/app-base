#include <Arduino.h>
#include <WiFiUdp.h>

#ifdef __PLATFORM_ESP8266
#include <ESP8266mDNS.h>
#endif
#ifdef __PLATFORM_ESP32
#include <ESPmDNS.h>
#endif

#include <MQTTClient.h>
#include <userapp.hpp>

#include <ArduinoOTA.h>
#ifndef __ARDUINO_OTA_PORT
#define __ARDUINO_OTA_PORT 8266
#endif

#include <modules/debug-utils/Schedule.hpp>

#include "application.hpp"
#include "applicationState.hpp"
#include <modules/configuration-manager/configuration-manager.hpp>
#include <modules/debug-utils/debug-utils.hpp>
#include <modules/debug-utils/debug-hook.hpp>
#include <modules/wifi-beacon/wifi-beacon.hpp>
#include <modules/mqtt-base/MQTTBase.hpp>
#include <modules/debug-utils/Logger.hpp>
#include <modules/debug-utils/Net-Logger.hpp>

MQTTBase *mqttBase;
Logger *appLogger = new Logger("app:core", 3, C_BLUE);
Logger *wifiLogger = new Logger("wifi:core", 3, C_CYAN);
Logger *mqttLogger = new Logger("mqtt:core", 3, C_GREEN);
Logger *otaLogger = new Logger("ota:core", 3, C_YELLOW);

bool connect_mqtt = false;
bool initialized = false;
uint32_t __app_last = 0;
uint8_t __app_updateRunning = 0;
uint8_t __app_mqtt_retries = 0;
Schedule *mqttConnectSchedule = new Schedule(1000);
Schedule *wifiStatusSchedule = new Schedule(100);

#if (defined(__PLATFORM_ESP32) || defined(__PLATFORM_ESP8266))
#include <modules/debug-utils/hooks/netlogger/NetLogger-XTensa.hpp>
NetLoggerXTensaHook *xTensaDebugHook = nullptr;
#endif

#ifdef __HW_M5STACK
#include <modules/debug-utils/hooks/m5/DisplayHook.hpp>
m5DisplayHook *m5StackHook = nullptr;
#endif

#include <modules/debug-utils/hooks/serial/ArduinoSerialHook.hpp>
ArduinoHWSerialHook *hwSerialHook = nullptr;

void application_preHookSetup(void){
#if (defined(__PLATFORM_ESP32) || defined(__PLATFORM_ESP8266))
	xTensaDebugHook = new NetLoggerXTensaHook(1337, 4);
	DebugHook_register(xTensaDebugHook);
#endif
#ifdef __HW_M5STACK
	m5StackHook = new m5DisplayHook();
	DebugHook_register(m5StackHook);
#endif
	hwSerialHook = new ArduinoHWSerialHook(&Serial);
	DebugHook_register(hwSerialHook);
	userapp_preHookSetup();
}

void application_setup() {
	mqttBase = new MQTTBase( String( CFG_getMqttServer() ), CFG_getMqttPort(), String( CFG_getMqttBaseTopic() ) );
	userapp_setup(mqttBase);
	application_state_get()->advanceToState(ESPCoreState::SETUP);
}

void application_loop() {
	DebugHook_tick();
	if (!initialized) {
		initialized = true;
		appLogger->printRaw("\n");
		appLogger->printf("Loading default config.\n");
		CFG_loadDefaults();
		appLogger->printf("Starting wifimanager ...\n");
		wifi_beacon_setup();
		application_setup();
		connect_mqtt = (CFG_getMqttPort() != 0);
		if (connect_mqtt == false) {
			appLogger->printf("ERROR! MQTT SETTINGS NOT SET!\n");
		}
		appLogger->printf("Connecting Wifi...\n");
	}

	wifi_beacon_loop();
	//if( wifiStatusSchedule->execute() )
	//	_printf( "\rWiFi Status: %u / App Status: %u", WiFi.status(), ESP_State);

	switch (application_state_get()->getCurrentState()) {
		case ESPCoreState::WIFI_CONNECTED:
		{
			wifiLogger->printf("Connected\n");
			wifiLogger->printf( 1, "IP address: %s\n", WiFi.localIP().toString().c_str() );
			wifiLogger->printf( 1, "MQTTServer: %s\n", CFG_getMqttServer() );
			wifiLogger->printf( 1, "MQTTPort: %i\n", CFG_getMqttPort() );
			wifiLogger->printf( 1, "MQTT Topic: %s\n", CFG_getMqttBaseTopic() );

			if (connect_mqtt) {
				application_state_get()->advanceToState(ESPCoreState::MQTT_CONNECTING);
				mqttBase->connect( String( CFG_getClientID() ) );

				char hostname[14];
#ifdef __PLATFORM_ESP8266
				uint32_t id = ESP.getChipId();
#elif defined(__PLATFORM_ESP32)
				uint64_t id = ESP.getEfuseMac();
#endif
				sprintf_P(hostname, PSTR("esp%lu"), id);
//#ifdef __PLATFORM_ESP8266
				if ( !MDNS.begin(hostname) ) {
					appLogger->printfln( "%sMDNS responder failed!%s", T_FG_COLOR(C_RED), T_MOD(T_RESET) );
				} else	 {
					appLogger->printfln( "%sMDNS responder successfully started!%s", T_FG_COLOR(C_GREEN), T_MOD(T_RESET) );
				}

				ArduinoOTA.setHostname(hostname);
				ArduinoOTA.setPort(__ARDUINO_OTA_PORT);
				ArduinoOTA.onStart([] () {
					otaLogger->printf("Initiated software update\n");
					__app_updateRunning = 1;
				});
				ArduinoOTA.onEnd([] () {
					otaLogger->printf("Update finished. Rebooting ...\n\n");
				});
				ArduinoOTA.onProgress([] (unsigned int progress, unsigned int total) {
					otaLogger->printf( "Progress: %u%%\r", ( progress / (total / 100) ) );
				});
				ArduinoOTA.onError([] (ota_error_t error) {
					otaLogger->printf("Error[%u]: ", error);
					__app_updateRunning = 0;
					if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
					else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
					else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
					else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
					else if (error == OTA_END_ERROR) Serial.println("End Failed");
				});

				ArduinoOTA.begin();

				otaLogger->printfln("Connect successful! MDNS Name: %s.local", hostname);
//#endif
				mqttLogger->println("Initiated connection to mqtt broker.");
				mqttLogger->println("Connecting");
				DebugHook_onConnectionEstablished();
			} else	{
				application_state_get()->advanceToState(ESPCoreState::MQTT_NOT_SET);
			}
			break;
		}

		case ESPCoreState::MQTT_CONNECTING:
		{
//#ifdef __PLATFORM_ESP8266
			ArduinoOTA.handle();
			if (__app_updateRunning == 1) {
				break;
			}
//#endif
			mqttBase->loop();
			if ( !mqttBase->connected() ) {
				if ( mqttConnectSchedule->execute() ) {
					__app_mqtt_retries++;
					if (__app_mqtt_retries >= MQTT_MAX_CONNECTION_TRIES) {
						application_state_get()->advanceToState(ESPCoreState::MQTT_FAILED);
						__app_mqtt_retries = 0;
					}
				}
			}
			break;
		}

		case ESPCoreState::MQTT_CONNECTED:
		{
			appLogger->printf("WIFI and MQTT connected!\n");
			application_connected();
			application_state_get()->advanceToState(ESPCoreState::RUNNING);
			appLogger->printf("Setting application to running\n");
			break;
		}

		case ESPCoreState::RUNNING:
		{
			if (millis() - __app_last > 10)	{
				__app_last = millis();
				if (__app_updateRunning == 0) mqttBase->timerISR();
			}
			if ( __app_last > millis() ) {
				__app_last = millis();
			}
//#ifdef __PLATFORM_ESP8266
			ArduinoOTA.handle();
//#endif
			if (__app_updateRunning == 0) {
				mqttBase->loop();
				userapp_loop();
			}
			break;
		}

		default:
			// Ups
			break;
	}
}

void application_disconnected(void) {
	appLogger->printf("Application is now disconnected!\n");
	DebugHook_onConnectionEnd();
	userapp_disconnected();
}

void application_connected(void){
	appLogger->printf("Application is now connected!\n");
	DebugHook_onConnectionEstablished();
	userapp_connected();
}