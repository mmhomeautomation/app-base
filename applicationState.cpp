#include "applicationState.hpp"

StateMachine<ESPCoreState::ESPState_t> *espDeviceState = nullptr;
ESPCoreState::StateDecoder *espDeviceStateDecoder = nullptr;

StateMachine<ESPCoreState::ESPState_t> *application_state_get(void){
	if(espDeviceState == nullptr){
		application_state_init();
	}
	return espDeviceState;
}

void application_state_init(void){
	espDeviceState = new StateMachine<ESPCoreState::ESPState_t>(ESPCoreState::NOT_SET, "esp");
	espDeviceStateDecoder = new ESPCoreState::StateDecoder();
	espDeviceState->setStateMachineDecoder(espDeviceStateDecoder);
}