#ifndef __APPLICATION_HPP__
#define __APPLICATION_HPP__

#include <inttypes.h>
#include <Arduino.h>

#include "applicationState.hpp"

#define MQTT_MAX_CONNECTION_TRIES 5

void application_preHookSetup(void);
void application_setup(void);
void application_main(void);
void application_loop(void);
void application_WifiConnected(void);
void application_connected(void);
void application_disconnected(void);

#endif
