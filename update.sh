#!/bin/bash
MODULES=( )

git_q() {
	git "$@" </dev/null >/dev/null 2>/dev/null
}

#
#	$1: local folder name
#	$2: remote repo name
#	$3: remote base url
#	$4: remote branch (master of not sure)
#	-> e.g.: $1 = eeprom-utils $2 = marcel/eeprom-utils $3 = gitlab_ccmob_ssl
#	-> ssh://git@git.ccmob.net:9022/marcel/eeprom-utils into src/modules/eeprom-utils
#
function addModule {
	MODULES+=("$1;$2;$3;$4")
}

function clone {
	echo "	Creating submodule $1 -> src/modules/$2"
	#git clone $1 src/modules/$2
	git submodule add $1 src/modules/$2
}

function getFile {
	wget -q "$1" -O "$2"
}

function checkFile {
	printf "Checking for file '$1'..."
	if [ ! -f "$1" ]; then
		printf " Downloading..."
		getFile $2 $1
	fi
	echo " OK"
}

function execute {
	cwd=$(pwd)
	echo "Current working dir: $cwd"

	if [ ! -d "src/modules" ]; then
		mkdir -p src/modules
	fi

	printf "Checking platformio.ini ..."
	if [ ! -f "platformio.ini" ]; then
		printf "not found. Loading file $2..."
		getFile "https://bitbucket.org/mmhomeautomation/app-base/raw/master/platformio.ini" "platformio.ini"
		echo " OK"
		echo "Initializing pio env"
		pio update && sudo pio upgrade
	else
		echo " OK"
	fi

	checkFile "src/userapp.cpp" "https://bitbucket.org/mmhomeautomation/app-base/raw/master/userapp.cpp.default"
	checkFile "src/userapp.hpp" "https://bitbucket.org/mmhomeautomation/app-base/raw/master/userapp.hpp.default"


	for ((i = 0; i < ${#MODULES[@]}; i++)); do
		module_localpath=$(echo ${MODULES[$i]} | cut -d';' -f1)
		module_remote_end=$(echo ${MODULES[$i]} | cut -d';' -f2)
		base_url=$(echo ${MODULES[$i]} | cut -d';' -f3)
		branch=$(echo ${MODULES[$i]} | cut -d';' -f4)
		remote_path=$base_url$module_remote_end
		echo "Checking module '$module_localpath'"
		if [ ! -d "src/modules/$module_localpath" ]; then
			echo "	Loading..."
			clone $remote_path $module_localpath
			cd src/modules/$module_localpath && git checkout $branch
			echo "	done."
			cd $cwd
		else
			echo "	OK. Updating to latest version..."
			cd src/modules/$module_localpath && git_q checkout $branch && git_q pull
			echo "	done."
			cd $cwd
		fi
	done
}

# Base urls
gitlab_ccmob_ssh="git@git.ccmob.net:"
gitlab_ccmob_https="https://git.ccmob.net/"
bitbucket_mm_ssh="git@bitbucket.org:mmhomeautomation/"

# addModule localname remote name baseurl (baseurl + remote = remoteurl)
addModule app-base app-base $bitbucket_mm_ssh master
addModule eeprom-utils marcel/eeprom-utils $gitlab_ccmob_ssh master
addModule configuration-manager configuration-manager $bitbucket_mm_ssh master
addModule debug-utils debug-utils $bitbucket_mm_ssh master
addModule wifi-beacon wifi-beacon $bitbucket_mm_ssh master
addModule wifi-manager wifimanager $bitbucket_mm_ssh master
addModule mqtt-base mqtt-base $bitbucket_mm_ssh syscfg
addModule aJson marcel/aJson $gitlab_ccmob_ssh master
addModule wifi-info marcel/wifi-info $gitlab_ccmob_ssh master
addModule mqtt-syscfg marcel/mqtt-syscfg $gitlab_ccmob_ssh master

# Sensors
#addModule mqtt-bme280 mqtt-bme280 $bitbucket_mm_ssh master
#addModule mqtt-ds18b20 mqtt-ds18b20 $bitbucket_mm_ssh master

#addModule lipo-info marcel/lipo-info $gitlab_ccmob_ssh master

# Output / Inputs
# Pwm module
#addModule mqtt-pwm mqtt-pwm $bitbucket_mm_ssh master
# GPIO on/off module
#addModule mqtt-output mqtt-output $bitbucket_mm_ssh master
# Button / input module
#addModule mqtt-button mqtt-button $bitbucket_mm_ssh master

execute