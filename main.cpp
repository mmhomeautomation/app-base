#include <modules/debug-utils/Logger.hpp>
#include <modules/debug-utils/debug-hook.hpp>
#include <modules/configuration-manager/configuration-manager.hpp>
#include "main.hpp"
#include "application.hpp"

void setup(void) {
	#ifdef __CORE_ON_IO
	pinMode(__CORE_ON_IO, OUTPUT);
	digitalWrite(__CORE_ON_IO, HIGH);
	#endif
	Serial.begin(921600);
	DebugHook_init();
	application_preHookSetup();
	DebugHook_initHooks();
	Logger_init(true);
	delay(1000);
}

void loop(void) {
	application_loop();
}
