TTY=#--port /dev/tty.SLAB_USBtoUART
BAUD=--baud 921600
#ENV=esp07
#ENV=esp32
ENV=m5stack-core-esp32

CC = pio

all: build

build:
	$(CC) run -e $(ENV) $(SILENT)

clean:
	$(CC) run -t clean -e $(ENV) $(SILENT)

burn_flash:
	$(CC) run -t upload -e $(ENV) $(SILENT)

serial:
	$(CC) device monitor $(BAUD) $(TTY) --rts 0 --dtr 0 --filter direct

burn: build burn_flash

run: burn serial

.PHONY : all
