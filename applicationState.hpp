#ifndef __APP_STATE__
#define __APP_STATE__

#include <modules/debug-utils/statemachine/StateMachine.hpp>

#define _CONCAT(a) #a
#define _CASE(switchCase) case switchCase: {return _CONCAT(switchCase); break;}

namespace ESPCoreState {
	typedef enum ESPState_e {
		NOT_SET,
		SETUP,
		WIFI_DISCONNECTED,
		WIFI_CONNECTING,
		WIFI_CONNECTED,
		MQTT_NOT_SET,
		MQTT_FAILED,
		MQTT_CONNECTING,
		MQTT_CONNECTED,
		RUNNING
	} ESPState_t;

	class StateDecoder : public StateMachineDecoder<ESPState_t> {
		public:
			const char* convertToString(ESPState_t s) override {
				switch(s){
					_CASE(NOT_SET)
					_CASE(SETUP)
					_CASE(WIFI_DISCONNECTED)
					_CASE(WIFI_CONNECTING)
					_CASE(WIFI_CONNECTED)
					_CASE(MQTT_FAILED)
					_CASE(MQTT_CONNECTING)
					_CASE(MQTT_CONNECTED)
					_CASE(RUNNING)
					default: return "INVALID";
				}
			}
	};
}

StateMachine<ESPCoreState::ESPState_t> *application_state_get(void);
void application_state_init(void);

#endif