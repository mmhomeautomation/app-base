app-base
___

This repo holds templates and files which are used in every project.

Use this script to start a project:

```sh
PRJ_NAME="My-Project"
mkdir $PRJ_NAME
cd $PRJ_NAME
git init
wget "https://bitbucket.org/mmhomeautomation/app-base/raw/master/update.sh" -q -O update.sh && chmod +x update.sh && ./update.sh
```
